#pragma once
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>

#include <Camera.hpp>

class CameraManager {
	std::vector<Camera> cameras;
	GLFWwindow* g_pWindow;
	GLuint programID;

	int activeCamera;

public:
	CameraManager(GLFWwindow* g_pWindow, GLuint programID);
	void addCamera(Camera *camera);
	void addCamera(glm::vec3 &camera_position, glm::vec3 &camera_lookAt);
	void addCamera(glm::vec3 & camera_position, glm::vec3 & camera_lookAt, float FoV, float aspect_ratio, float near, float far);
	Camera* getCamera(int i);
	Camera* getCamera();
	void setActiveCamera(int i);
};