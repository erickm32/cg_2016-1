#pragma once
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>

class Camera {
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
	GLuint viewMatrixID;

	GLFWwindow* g_pWindow;

	// Initial position : on +Z
	glm::vec3 position; // glm::vec3(0, 0, 5);
	// Initial horizontal angle : toward -Z //3.14f
	float horizontalAngle; 
	// Initial vertical angle : none //0.0f
	float verticalAngle; 
	// Initial Field of View // 45.0f
	float initialFoV; 

	float speed; // 3.0f
	float mouseSpeed; // 0.005f

	float near;
	float far;
	float aspect_ratio;

	glm::vec3 direction;
	glm::vec3 up;

public:
	Camera();
	Camera(GLuint viewMatrixID);
	Camera(GLFWwindow* g_pWindow, GLuint viewMatrixID);
	Camera(GLFWwindow* g_pWindow, GLuint viewMatrixID, glm::vec3 &camera_position, glm::vec3 &camera_lookAt);
	Camera(GLFWwindow* g_pWindow, GLuint viewMatrixID, glm::vec3 &camera_position, glm::vec3 &camera_lookAt, float FoV, float aspect_ratio, float near, float far);
	void sendValuesToGPU();
	glm::mat4 *getViewMatrix();
	void setViewMatrix(glm::mat4 &viewMatrix);
	glm::mat4 *getProjectionMatrix();
	void setProjectionMatrix(glm::mat4 &projectionMatrix);

	void computeMatricesFromInputs(int nUseMouse, int nWidth, int nHeight);

	void setPositionAndLook(glm::vec3 & positionToTranslate, glm::vec3 & positionToLookAt);
	void setRotation(glm::mat4 &mat);
	void setUpvector(glm::vec3 upVector);
	void setFoV(float FoV);

};
