#pragma once
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>
using namespace glm;

#include <shader.hpp>
#include <texture.hpp>
#include <controls.hpp>
#include <objloader.hpp>
#include <vboindexer.hpp>
#include <glerror.hpp>

#include <glm/gtc/quaternion.hpp> 
#include <glm/gtx/quaternion.hpp>

#include <Mesh.hpp>

class Model {
	Mesh *mesh;
	glm::mat4 modelMatrix;
	GLuint ModelMatrixID;
	int id;
	

public:
	Model();
	Model(const char  *obj, int id, GLuint programID);
	Model(Mesh* mesh, int id, GLuint programID);

	const int getId();

	void drawElements();

	Mesh* getMesh();

	void setMesh(Mesh *mesh);

	void setModelMatrix(const glm::mat4 &modelMatrix);

	glm::mat4 getModelMatrix();
};