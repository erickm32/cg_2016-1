#include <vector>
#include <unordered_map>
#include <string>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include AntTweakBar
#include <AntTweakBar.h>
/*
TwBar *g_pToolBar;
TwBar *cameraToolBar;
TwBar *animationToolBar;
*/

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>
using namespace glm;

#include <ModelManager.hpp>
#include <CameraManager.hpp>

struct ModelValues {
	glm::vec3 translateValues = glm::vec3(0.1f, 0.1f, 0.1f);
	glm::vec3 scaleValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_scaleUniform = false;
	float quat_bar[4] = { 0, 0, 0, 1 };
	glm::vec3 shear_xValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_shear_x = false;
	glm::vec3 shear_yValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_shear_y = false;
	glm::vec3 shear_zValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_shear_z = false;
	glm::vec3 pointForTheArbitraryAxisValues = glm::vec3(1.0f, 0.0f, 0.0f);
	double angleForArbitraryAxis = 0.0;
	bool bool_reflectX = false;
	bool bool_reflectY = false;
	bool bool_reflectZ = false;
};

struct CameraValues {
	glm::vec3 translateValues = glm::vec3(0.1f, 0.1f, 0.1f);
	float camera_quat_bar[4] = { 0, 0, 0, 1 };
	glm::vec3 LookAtValues = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 pointForTheArbitraryAxisValues = glm::vec3(1.0f, 0.0f, 0.0f);
	double angleForArbitraryAxis = 0.0;
	float cameraFoV = 45.0f;
};

struct ModelToolbar{
	TwBar *toolbar;
	ModelValues *values;
};

struct CameraToolbar {
	TwBar *toolbar;
	CameraValues *values;
};

void TW_CALL addSuzanne(void *clientData);
void TW_CALL addGoose(void *clientData);
void TW_CALL addCube(void *clientData);
void TW_CALL addCamera(void *clientData);

class ToolBarManager {
	//std::vector<TwBar*> toolbars;
	std::vector<ModelToolbar> modelToolbars;
	std::vector<CameraToolbar> cameraToolbars;

	TwBar *menu;
	//std::unordered_map<std::string, TwBar*> toolbars;
	ModelManager *modelManager;
	CameraManager *cameraManager;

	//TO DO
	// fazer toda estrutura de callback de windowResize
public:
	ToolBarManager(ModelManager *modelManager, CameraManager *cameraManager);//criar menu aqui

	void addNewModel(const char* obj);
	void addNewCamera(glm::vec3 & camera_position, glm::vec3 & camera_lookAt, float FoV, float aspect_ratio, float near, float far);
	TwBar* getToolBar();
	void draw();
};