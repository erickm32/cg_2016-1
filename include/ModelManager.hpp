#pragma once
#include <vector>
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>
using namespace glm;

#include <shader.hpp>
#include <texture.hpp>
#include <controls.hpp>
#include <objloader.hpp>
#include <vboindexer.hpp>
#include <glerror.hpp>

#include <glm/gtc/quaternion.hpp> 
#include <glm/gtx/quaternion.hpp>

#include <Mesh.hpp>
#include <Model.hpp>
#include <ModelManager.hpp>
#include <Camera.hpp>

class ModelManager {
	std::vector<Model> models;
	std::vector<Mesh*> meshes;

	GLuint programID;

	Mesh* findMesh(std::string &meshId);

public:

	ModelManager(GLuint programID) {
		this->programID = programID;
	}

	void addModel(const char  *obj);
	void deleteModel(int &id);
	void drawElements();
	void terminate();
	glm::mat4 getModelMatrix(int id);
	void setModelMatrix(int id, glm::mat4 &modelMatrix);
	Model *getModel(int id);
	const int size();
	void changeModelMesh(int modelId, const char  *meshId);
};