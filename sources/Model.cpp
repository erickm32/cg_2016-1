#pragma once
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>

#include <Model.hpp>

Model::Model() {
	this->mesh = NULL;
	id = -1;
	this->modelMatrix = glm::mat4(1.0);
}
Model::Model(const char  *obj, int id, GLuint programID) {
	this->mesh = new Mesh(obj, programID);
	this->id = id;
	this->modelMatrix = glm::mat4(1.0);
	ModelMatrixID = glGetUniformLocation(programID, "M");
}
Model::Model(Mesh* mesh, int id, GLuint programID) {
	this->mesh = mesh;
	this->id = id;
	this->modelMatrix = glm::mat4(1.0);
	ModelMatrixID = glGetUniformLocation(programID, "M");
}

const int Model::getId() {
	return id;
}

void Model::drawElements() {
	mesh->drawElements();
}

Mesh* Model::getMesh() {
	return mesh;
}

void Model::setMesh(Mesh *mesh) {
	this->mesh = mesh;
}

void Model::setModelMatrix(const glm::mat4 &modelMatrix) {
	this->modelMatrix = modelMatrix;
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
}

glm::mat4 Model::getModelMatrix() {
	return modelMatrix;
}
