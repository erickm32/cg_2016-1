#include <ToolBarManager.hpp>

ToolBarManager::ToolBarManager(ModelManager * modelManager, CameraManager *cameraManager) {
	this->modelManager = modelManager;
	this->cameraManager = cameraManager;

	TwInit(TW_OPENGL_CORE, NULL);

	menu = TwNewBar("CG_UFPel Menu");
	TwAddButton(menu, "adicionarSuzanne", addSuzanne, this, " label='Adicionar Macaco' ");
	//TwAddButton(menu, "adicionarSuzanne", []() -> void TW_CALL{}, this, " label='Adicionar Macaco' ");
	TwAddButton(menu, "adicionarGoose", addGoose, this, " label='Adicionar Ganso' ");
	TwAddButton(menu, "adicionarCube", addCube, this, " label='Adicionar Cubo' ");
	TwAddSeparator(menu, "separator4", NULL);
	TwAddButton(menu, "adicionarcamera", addCamera, this, " label='Adicionar Camera' ");
	TwDefine("'MENU PRINCIPAL' size='240 300'");

}

void TW_CALL addSuzanne(void *clientData) {
	auto ptr = (ToolBarManager*)clientData;
	ptr->addNewModel("mesh/suzanne.obj");
}

void TW_CALL addGoose(void *clientData) {
	auto ptr = (ToolBarManager*)clientData;
	ptr->addNewModel("mesh/goose.obj");
}

void TW_CALL addCube(void *clientData) {
	auto ptr = (ToolBarManager*)clientData;
	ptr->addNewModel("mesh/cube.obj");
}

void TW_CALL addCamera(void *clientData) {
	auto ptr = (ToolBarManager*)clientData;
	//ptr->addNewCamera(camera_position, camera_lookAt, FoV, aspect_ratio, near, far);
}

void ToolBarManager::addNewModel(const char* obj) {
	this->modelManager->addModel(obj);
}

void ToolBarManager::addNewCamera(glm::vec3 & camera_position, glm::vec3 & camera_lookAt, float FoV, float aspect_ratio, float near, float far) {
	this->cameraManager->addCamera(camera_position, camera_lookAt, FoV, aspect_ratio, near, far);
}

TwBar * ToolBarManager::getToolBar() {
	return nullptr;
}

void ToolBarManager::draw() {
	TwDraw();
}
