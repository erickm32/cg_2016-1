// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <unordered_set>
#include <queue>
#include <iostream>
#include <fstream>
#include <math.h>
#include <string>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* g_pWindow;
unsigned int g_nWidth = 1024, g_nHeight = 768;

// Include AntTweakBar
#include <AntTweakBar.h>
TwBar *g_pToolBar;
TwBar *cameraToolBar;
TwBar *cameraToolBar2;
TwBar *animationToolBar;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>
using namespace glm;

#include <shader.hpp>
#include <texture.hpp>
#include <controls.hpp>
#include <objloader.hpp>
#include <vboindexer.hpp>
#include <glerror.hpp>

#include <glm/gtc/quaternion.hpp> 
#include <glm/gtx/quaternion.hpp>

#include <Mesh.hpp>
#include <Model.hpp>
#include <ModelManager.hpp>
#include <Camera.hpp>
#include <CameraManager.hpp>


void WindowSizeCallBack(GLFWwindow *pWindow, int nWidth, int nHeight) {

	g_nWidth = nWidth;
	g_nHeight = nHeight;
	glViewport(0, 0, g_nWidth, g_nHeight);
	TwWindowSize(g_nWidth, g_nHeight);
}


std::vector<glm::vec3> *ref_indexed_vertices;
struct Data {
	int pVertex = -1;
	std::vector<int> neighbor_vertices;
	std::vector<float> neighbor_distances;
	int qntNeighbors = 0;

	Data(const int &pVertex) {
		this->pVertex = pVertex;
	}

	Data() {
	}
	//opera��es O(n), pq nao consegui fazer funcionar unordered_set ou unordered_map
	void insert(const int &pVertex) {
		int flag = 0;
		for (int v : neighbor_vertices) {
			if (v == pVertex) {
				flag = 1;
			}
		}
		if (flag == 0) {
			neighbor_vertices.push_back(pVertex);
			neighbor_distances.push_back(fabs(glm::distance((*ref_indexed_vertices)[this->pVertex], (*ref_indexed_vertices)[pVertex])));
			qntNeighbors++;
		}
	}
	/*
	Estou no ponto a ser removido.
	Acho o ponto com menor distancia e passo pra ca
	as distancias serao atualizadas com a distancia dele pros outros*/
	void updateDistances(const int &index, const int &pVertex) {
		for (int i = 0; i < neighbor_vertices.size(); i++) {
			neighbor_distances[i] = fabs(glm::distance((*ref_indexed_vertices)[index], (*ref_indexed_vertices)[pVertex]));
		}
	}
	void updateNeighborsCount() {
		std::unordered_set<int> s;
		for (int i = 0; i < neighbor_vertices.size(); i++) {
			s.insert(neighbor_vertices[i]);
		}
		//std::cout << s.size() << std::endl;
	}
	/*Acabou que nem precisa disso aqui*/
	void remove(const int &pVertex){
		int flag = -1;
		for (int i = 0; i < neighbor_vertices.size(); i++) {
			if (neighbor_vertices[i] == pVertex) {
				flag = i;
			}
		}
		if (flag >= 0) {
			auto it = neighbor_vertices.begin();
			neighbor_vertices.erase(it + flag);
			auto it2 = neighbor_distances.begin();
			neighbor_distances.erase(it2 + flag);
			qntNeighbors--;
		}
	}
	//(coloquei o reverse numa tentativa de implementar a volta dos vertices que nao deu muito certo)
	int getShortestDistance(bool reverse = false) {
		if (reverse == false) {
			float d = 999;
			int i, r;
			for (i = 0; i < neighbor_distances.size(); i++) {
				if (neighbor_distances[i] <= d) {
					d = neighbor_distances[i];
					r = i;
				}
			}
			//std::cout << r << std::endl;
			return r;
		}
		else {
			/*biggest na verdade*/
			float d = -1;
			int i, r;
			for (i = 0; i < neighbor_distances.size(); i++) {
				if (neighbor_distances[i] >= d) {
					d = neighbor_distances[i];
					r = i;
				}
			}
			//std::cout << r << std::endl;
			return r;
		}
		
	}
};

// fun��o de compara��o para a fila de prioridade
// segue os moldes das encontradas na internet, s� adaptei pro meu caso
class CompareQntNeighbors {
	bool reverse;
public:
	CompareQntNeighbors(const bool& revparam = false) {
		reverse = revparam;
	}
	bool operator() (const Data& lhs, const Data&rhs) const {
		if (reverse) return ( lhs.qntNeighbors > rhs.qntNeighbors);
		else return (lhs.qntNeighbors < rhs.qntNeighbors);
	}
};

// which shear to be applied 
//if more than one or no bool is true, returns -1, otherwise 
//0 for x, 1 for y or 2 for z
// (t� em ingles, mas fui eu que fiz mesmo haha, era em uma hora bem diferente hahaha)
int whichShear(bool x, bool y, bool z) {
	if (x) {
		if (y) {
				return -1;
		}
		else {
			if (z) {
				return -1;
			}
			else {
				return 0; // x = true y = false z = false
			}
		}
	}
	else {
		if (y) {
			if (z) {
				return -1;
			}
			else {
				return 1; // x = false y = true z = false
			}
		}
		else {
			if (z) {
				return 2; // x = false y= false z = true
			}
			else {
				return -1;
			}
		}
	}
}

// Mesma ideia da de cima
int whichReflection(bool x, bool y, bool z) {
	if (x) {
		if (y) {
			return -1;
		}
		else {
			if (z) {
				return -1;
			}
			else {
				return 0; // x = true y = false z = false
			}
		}
	}
	else {
		if (y) {
			if (z) {
				return -1;
			}
			else {
				return 1; // x = false y = true z = false
			}
		}
		else {
			if (z) {
				return 2; // x = false y= false z = true
			}
			else {
				return -1;
			}
		}
	}
}


// Matrizes utilizadas para refletir os vertices nos planos XY, XZ, YZ
// Baseads em:
// http://folk.uio.no/martinre/inf3320-2011/lecture4.pdf
// http://www.cs.brandeis.edu/~cs155/Lecture_07_6.pdf
// e no livro fundamentals of computer graphics
// n�o consegui fazer funcionar corretamente ou ao menos como esperava que seria
glm::mat4 reflectionXY() {
	return glm::mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
}
glm::mat4 reflectionXZ() {
	return glm::mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, -1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
}
glm::mat4 reflectionYZ() {
	return glm::mat4(
		-1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
}

// tentativa de fazer uma rota��o em um vetor qualqer. deveria receber o vetor qualquer e um angulo
// foi baseado nos pdf seguintes
// http://folk.uio.no/martinre/inf3320-2011/lecture4.pdf
// http://www.cs.brandeis.edu/~cs155/Lecture_07_6.pdf
// estavam ocorrendo distor��es, na maioria das vezes.
// depois descobri a fun��o da glm com entrada igual que funcionou
glm::mat4 rotationArbitraryAxis(double angle, glm::vec3 r) {
	if (r.length() > 1) {
		r = normalize(r);
	}
	angle = radians(angle);
	return glm::mat4(
		cos(angle) + (1 - cos(angle))*r.x*r.x, (1 - cos(angle))*r.x*r.y - r.x*sin(angle), (1 - cos(angle))*r.x*r.z + r.y*sin(angle), 0.0f,
		(1 - cos(angle))*r.x*r.y + r.z*sin(angle), cos(angle) + (1 - cos(angle))*r.y*r.y, (1 - cos(angle))*r.y*r.z - r.x*sin(angle), 0.0f,
		(1 - cos(angle))*r.x*r.z - r.y*sin(angle), (1 - cos(angle))*r.y*r.z + r.x*sin(angle), cos(angle) + (1 - cos(angle))*r.z*r.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
}

// tentativa de criar uma translate, n�o entendi o motivo, mas saia tudo completamente errado
// utilizei a da glm depois
glm::mat4 translado(glm::vec3 &v) {
	return glm::mat4(
		1.0f, 0.0f, 0.0f, v.x,
		0.0f, 1.0f, 0.0f, v.y,
		0.0f, 0.0f, 1.0f, v.z,
		0.0f, 0.0f, 0.0f, 1.0f
		);
}

// http://www.ogldev.org/www/tutorial48/tutorial48.html
// Create an internal enum to name the meshes, como visto no tutorial acima
typedef enum { SUZANNE, CUBE, GOOSE } MESH_TYPE;

//Uma struct que serviria pra guardar os valores utilizados em cada objeto, mas resolvi desistir de fazer isso
struct Toolbar_values {
	glm::vec3 scaleValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_scaleUniform = false;

	glm::vec3 translateValues = glm::vec3(0.1f, 0.1f, 0.1f);
	bool bool_animatedTranslate = false;

	float quat_bar[4] = { 0, 0, 0, 1 }; // null rotation

	glm::vec3 shear_xValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_shear_x = false;

	glm::vec3 shear_yValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_shear_y = false;

	glm::vec3 shear_zValues = glm::vec3(1.0f, 1.0f, 1.0f);
	bool bool_shear_z = false;

	glm::vec3 pointForTheArbitraryAxisValues = glm::vec3(1.0f, 0.0f, 0.0f);
	double angleForArbitraryAxis = 0.0;

	bool bool_reflectX = false;
	bool bool_reflectY = false;
	bool bool_reflectZ = false;

	void setValues(
		glm::vec3 scaleValues, bool bool_scaleUniform, 
		glm::vec3 translateValues, bool bool_animatedTranslate,
		float quat_bar[4],
		glm::vec3 shear_xValues,
		bool bool_shear_x,
		glm::vec3 shear_yValues,
		bool bool_shear_y,
		glm::vec3 shear_zValues,
		bool bool_shear_z, 
		glm::vec3 pointForTheArbitraryAxisValues,
		double angleForArbitraryAxis,
		bool bool_reflectX,
		bool bool_reflectY,
		bool bool_reflectZ
		) {
		this->scaleValues = scaleValues;
		this->bool_scaleUniform = bool_scaleUniform;

		this->translateValues = translateValues;
		this->bool_animatedTranslate = bool_animatedTranslate;

		this->quat_bar[0] = quat_bar[0];
		this->quat_bar[1] = quat_bar[1];
		this->quat_bar[2] = quat_bar[2];
		this->quat_bar[3] = quat_bar[3];
		
		this->shear_xValues = shear_xValues;
		this->bool_shear_x = bool_shear_x;

		this->shear_yValues = shear_yValues;
		this->bool_shear_y = bool_shear_y;

		this->shear_zValues = shear_zValues;
		this->bool_shear_z = bool_shear_z;

		this->pointForTheArbitraryAxisValues = pointForTheArbitraryAxisValues;
		this->angleForArbitraryAxis = angleForArbitraryAxis;

		this->bool_reflectX = bool_reflectX;
		this->bool_reflectY = bool_reflectY;
		this->bool_reflectZ = bool_reflectZ;
	}
};
Toolbar_values toolbar_values[3];

/*
//tentativa de usar um callback pra tentar saber quando foi que o usuario selecionou outro modelo
//teria que modficar isso aqui, mas n�o achei que funcionaria como queria
void TW_CALL SetCallback(const void *value, void *clientData){
	myVariable = *(const MyVariableType *)value;  // for instance
}
*/


enum TransformationPriority{
	OTHER, TRANSLATION, ROTATION, SCALE
};
struct Transformation {
	glm::mat4 matrix;
	int priority;
};

void TW_CALL animationToolBarButtonCallback(void *clientData){
	if (clientData == NULL) {
	}

}

int main(void){
	
	int nUseMouse = 0;

	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	g_pWindow = glfwCreateWindow(g_nWidth, g_nHeight, "CG UFPel", NULL, NULL);
	if (g_pWindow == NULL){
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(g_pWindow);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	check_gl_error();//OpenGL error from GLEW

	// Initialize the GUI
	TwInit(TW_OPENGL_CORE, NULL);
	TwWindowSize(g_nWidth, g_nHeight);

	// Set GLFW event callbacks. I removed glfwSetWindowSizeCallback for conciseness
	glfwSetMouseButtonCallback(g_pWindow, (GLFWmousebuttonfun)TwEventMouseButtonGLFW); // - Directly redirect GLFW mouse button events to AntTweakBar
	glfwSetCursorPosCallback(g_pWindow, (GLFWcursorposfun)TwEventMousePosGLFW);          // - Directly redirect GLFW mouse position events to AntTweakBar
	glfwSetScrollCallback(g_pWindow, (GLFWscrollfun)TwEventMouseWheelGLFW);    // - Directly redirect GLFW mouse wheel events to AntTweakBar
	glfwSetKeyCallback(g_pWindow, (GLFWkeyfun)TwEventKeyGLFW);                         // - Directly redirect GLFW key events to AntTweakBar
	glfwSetCharCallback(g_pWindow, (GLFWcharfun)TwEventCharGLFW);                      // - Directly redirect GLFW char events to AntTweakBar
	glfwSetWindowSizeCallback(g_pWindow, WindowSizeCallBack);

	//create the toolbar
	g_pToolBar = TwNewBar("CG UFPel ToolBar");
	cameraToolBar = TwNewBar("CameraToolBar");
	cameraToolBar2 = TwNewBar("CameraToolBar2");

	// A variable for the current selection - will be updated by ATB
	MESH_TYPE m_currentMesh = SUZANNE;
	
	// Array of drop down items
	TwEnumVal Meshes[] = { { SUZANNE, "Suzanne" },{ CUBE, "Cube" },{ GOOSE, "Goose" } };

	// ATB identifier for the array
	TwType MeshTwType = TwDefineEnum("MeshType", Meshes, 3);

	// Link it to the tweak bar
	//TwAddVarCB(g_pToolBar, "Mesh", MeshTwType,NULL,NULL, &m_currentMesh, NULL); //tentativa de usar um callback pra tentar saber quando foi que o usuario selecionou outro modelo
	TwAddVarRW(g_pToolBar, "Mesh", MeshTwType, &m_currentMesh, NULL);

	// The second parameter is an optional name
	TwAddSeparator(g_pToolBar, "", NULL);
	/*
	// Add 'speed' to 'bar': it is a modifable (RW) variable of type TW_TYPE_DOUBLE. Its key shortcuts are [s] and [S].
	double speed = 0.0;
	TwAddVarRW(g_pToolBar, "speed", TW_TYPE_DOUBLE, &speed, " label='Rot speed' min=0 max=2 step=0.01 keyIncr=s keyDecr=S help='Rotation speed (turns/second)' ");
	*/

	// Add 'scale' to 'bar': it is a modifable (RW) variable of type TW_TYPE_DOUBLE. 
	glm::vec3 scaleValues(1.0f, 1.0f, 1.0f);
	TwAddVarRW(g_pToolBar, "scale", TW_TYPE_DIR3F, &scaleValues, " label='scale' help='scale values to be altered in the modelMatrix' group='Scale' ");
	bool bool_scaleUniform = false;
	TwAddVarRW(g_pToolBar, "bool_scaleUniform", TW_TYPE_BOOLCPP, &bool_scaleUniform, " label='Uniform Scale?' group='Scale'");

	// add translate values to bar
	glm::vec3 translateValues(0.1f, 0.1f, 0.1f);
	TwAddVarRW(g_pToolBar, "translate", TW_TYPE_DIR3F, &translateValues, " label='Translate Values' help='translate values to be altered in the modelMatrix' group='Translate'");
	bool bool_animatedTranslate = false;
	TwAddVarRW(g_pToolBar, "bool_animatedTranslate", TW_TYPE_BOOLCPP, &bool_animatedTranslate, " label='Animated Translate?' group = 'Translate'"); ;

	float quat_bar[4] = { 0, 0, 0, 1 }; // sem rota��o
	TwAddVarRW(g_pToolBar, "Rotation", TW_TYPE_QUAT4F, &quat_bar, "label = 'Rotation Point' help='Rotation values in a quaternion to be altered in the modelMatrix'");

	// shear-x
	glm::vec3 shear_xValues(1.0f, 1.0f, 1.0f);
	TwAddVarRW(g_pToolBar, "shear_xValues", TW_TYPE_DIR3F, &shear_xValues, " label='x' group='Shear' ");
	// bool shear-x 
	bool bool_shear_x = false;
	TwAddVarRW(g_pToolBar, "bool_shear_x", TW_TYPE_BOOLCPP, &bool_shear_x, " label='bool_x' group='Shear'");

	// shear-y
	glm::vec3 shear_yValues(1.0f, 1.0f, 1.0f);
	TwAddVarRW(g_pToolBar, "shear_yValues", TW_TYPE_DIR3F, &shear_yValues, " label='y' group='Shear' ");
	// bool shear-y 
	bool bool_shear_y = false;
	TwAddVarRW(g_pToolBar, "bool_shear_y", TW_TYPE_BOOLCPP, &bool_shear_y, " label='bool_y' group='Shear'");

	// shear-z
	glm::vec3 shear_zValues(1.0f, 1.0f, 1.0f);
	TwAddVarRW(g_pToolBar, "shear_zValues", TW_TYPE_DIR3F, &shear_zValues, " label='z' group='Shear'");
	// bool shear-z 
	bool bool_shear_z = false;
	TwAddVarRW(g_pToolBar, "bool_shear_z", TW_TYPE_BOOLCPP, &bool_shear_z, " label='bool_z' group='Shear'");

	// Select a point
	glm::vec3 pointForTheArbitraryAxisValues(1.0f, 0.0f, 0.0f);
	TwAddVarRW(g_pToolBar, "pointForTheArbitraryAxisValues", TW_TYPE_DIR3F, &pointForTheArbitraryAxisValues, " label='Ponto para o eixo' group='RotationAxis' ");
	double angleForArbitraryAxis = 0.0;
	TwAddVarRW(g_pToolBar, "angleForArbitraryAxis", TW_TYPE_DOUBLE, &angleForArbitraryAxis, " label='Angulo para o eixo'  group='RotationAxis' "); //step='0.01'

	
	bool bool_reflectX = false;
	TwAddVarRW(g_pToolBar, "bool_reflectX", TW_TYPE_BOOLCPP, &bool_reflectX, " label='reflectX' group='Reflection'");
	// bool shear-z 
	bool bool_reflectY = false;
	TwAddVarRW(g_pToolBar, "bool_reflectY", TW_TYPE_BOOLCPP, &bool_reflectY, " label='reflectY' group='Reflection'");
	// bool shear-z 
	bool bool_reflectZ = false;
	TwAddVarRW(g_pToolBar, "bool_reflectZ", TW_TYPE_BOOLCPP, &bool_reflectZ, " label='reflectZ' group='Reflection'");

	//Toolbar da Camera
	glm::vec3 cameraTranslateValues = glm::vec3(0.0f, 0.0f, 5.0f);
	TwAddVarRW(cameraToolBar, "pontoCameraTranslate", TW_TYPE_DIR3F, &cameraTranslateValues, "label='Ponto Transla��o: '");
	glm::vec3 cameraLookAtValues = glm::vec3(0.0f, 0.0f, 0.0f);
	TwAddVarRW(cameraToolBar, "pontoCameraLookAt", TW_TYPE_DIR3F, &cameraLookAtValues, "label='Olhar para: '");
	float cameraFoV = 45.0f;
	TwAddVarRW(cameraToolBar, "cameraFoV", TW_TYPE_FLOAT, &cameraFoV, "label='Field of View: ' min='45' max='210'");
	float camera_quat_bar[4] = { 0, 0, 0, 1 }; // sem rota��o
	TwAddVarRW(cameraToolBar, "Rotation", TW_TYPE_QUAT4F, &camera_quat_bar, "label = 'Rodar em torno do ponto:' showval='true' ");
	glm::vec3 camera_pointForTheArbitraryAxisValues(1.0f, 0.0f, 0.0f);// Select a point
	TwAddVarRW(cameraToolBar, "pointForTheArbitraryAxisValues", TW_TYPE_DIR3F, &camera_pointForTheArbitraryAxisValues, " label='Ponto para o eixo' group='RotationAxis' ");
	double camera_angleForArbitraryAxis = 0.0;
	TwAddVarRW(cameraToolBar, "angleForArbitraryAxis", TW_TYPE_DOUBLE, &camera_angleForArbitraryAxis, " label='Angulo para o eixo'  group='RotationAxis' "); //step='0.01'
	/*
	TwAddSeparator(cameraToolBar, "separator", NULL);
	int activeCamera = 0;
	TwAddVarRW(cameraToolBar, "activeCameraSelector", TW_TYPE_INT32, &activeCamera, "label='Camera ativa:' min='0' ");
	*/
	//Toolbar da Camera2, muito porcamente
	glm::vec3 cameraTranslateValues2 = glm::vec3(0.0f, 0.0f, 5.0f);
	TwAddVarRW(cameraToolBar2, "pontoCameraTranslate2", TW_TYPE_DIR3F, &cameraTranslateValues2, "label='Ponto Transla��o: '");
	glm::vec3 cameraLookAtValues2 = glm::vec3(0.0f, 0.0f, 0.0f);
	TwAddVarRW(cameraToolBar2, "pontoCameraLookAt2", TW_TYPE_DIR3F, &cameraLookAtValues2, "label='Olhar para: '");
	float cameraFoV2 = 90.0f;
	TwAddVarRW(cameraToolBar2, "cameraFoV2", TW_TYPE_FLOAT, &cameraFoV2, "label='Field of View: ' min='45' max='210'");
	float camera_quat_bar2[4] = { 0, 0, 0, 1 }; // sem rota��o
	TwAddVarRW(cameraToolBar2, "Rotation2", TW_TYPE_QUAT4F, &camera_quat_bar2, "label = 'Rodar em torno do ponto:' showval='true' ");
	glm::vec3 camera_pointForTheArbitraryAxisValues2(1.0f, 0.0f, 0.0f);// Select a point
	TwAddVarRW(cameraToolBar2, "pointForTheArbitraryAxisValues2", TW_TYPE_DIR3F, &camera_pointForTheArbitraryAxisValues2, " label='Ponto para o eixo' group='RotationAxis' ");
	double camera_angleForArbitraryAxis2 = 0.0;
	TwAddVarRW(cameraToolBar, "angleForArbitraryAxis2", TW_TYPE_DOUBLE, &camera_angleForArbitraryAxis2, " label='Angulo para o eixo'  group='RotationAxis' "); //step='0.01'

		



	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(g_pWindow, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(g_pWindow, g_nWidth / 2, g_nHeight / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders("shaders/StandardShading.vertexshader", "shaders/StandardShading.fragmentshader");

	// Get a handle for our "MVP" uniform
	GLuint MatrixID      = glGetUniformLocation(programID, "MVP");


	ModelManager modelManager = ModelManager(programID);

	modelManager.addModel("mesh/suzanne.obj");
	modelManager.addModel("mesh/cube.obj");
	modelManager.addModel("mesh/goose.obj");
	modelManager.addModel("mesh/suzanne.obj");
	modelManager.addModel("mesh/suzanne.obj");
	//modelManager.addModel("mesh/pooltable.obj");
	//modelManager.setModelMatrix(5, glm::translate(5.0f, 2.0f, 0.0f));

	CameraManager cameraManager = CameraManager(g_pWindow, programID);

	Camera camera = Camera(g_pWindow, glGetUniformLocation(programID, "V"));
	
	cameraManager.addCamera(&camera);
	cameraManager.addCamera(glm::vec3(0.0f, 5.0f, 5.0f), glm::vec3(0.0f, 0.0f, 0.0f), 90.0f, 4.0f / 3.0f, 0.1f, 100.0f);

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

	// For speed computation
	double lastTime = glfwGetTime();
	int nbFrames    = 0;
	int activeCamera = 0;
	glm::mat4 teste = glm::translate(-3.0f, 1.0f, 0.0f);
	do{
        check_gl_error();

        //use the control key to free the mouse
		if (glfwGetKey(g_pWindow, GLFW_KEY_LEFT_CONTROL) != GLFW_PRESS)
			nUseMouse = 0;
		else
			nUseMouse = 1;

		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if (currentTime - lastTime >= 1.0){ // If last prinf() was more than 1sec ago
			// printf and reset
			printf("%f ms/frame fps: %d\n", 1000.0 / double(nbFrames), nbFrames);
			//std::cout << "m_cur: " << m_currentMesh << " &m_cur: " << &m_currentMesh << " tb: " << tb << " &tb: " << &tb << std::endl;
			nbFrames  = 0;
			lastTime += 1.0;

			if (glfwGetKey(g_pWindow, GLFW_KEY_SPACE) == GLFW_PRESS) {
				activeCamera++;
				cameraManager.setActiveCamera(activeCamera % 2);
			}

		}

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		
		//cameraManager.setActiveCamera(activeCamera);
		cameraManager.getCamera(0)->computeMatricesFromInputs(nUseMouse, g_nWidth, g_nHeight);
		cameraManager.getCamera(1)->computeMatricesFromInputs(nUseMouse, g_nWidth, g_nHeight);
		//camera.setProjectionMatrix(getProjectionMatrix());
		//camera.setViewMatrix(getViewMatrix());

		//todos esses valores eram a tentativa de uma grande gambiarra para usar as mesmas vari�veis para mexer com tres modelos alternadamente e sem influencia no outro
		//mas deixei de lado
		toolbar_values[m_currentMesh].setValues(scaleValues, bool_scaleUniform, translateValues, bool_animatedTranslate, quat_bar, shear_xValues, bool_shear_x, shear_yValues, bool_shear_y, shear_zValues, bool_shear_z, pointForTheArbitraryAxisValues, angleForArbitraryAxis, bool_reflectX, bool_reflectY, bool_reflectZ);
		auto tb = &(toolbar_values[m_currentMesh]); // s� pra ficar menos ilegivel 

		// Transla��o, desloca o centro do modelo para o ponto (x,y,z) abaixo
		glm::mat4 translatedMatrix = glm::translate(tb->translateValues.x, tb->translateValues.y, tb->translateValues.z);


		
		// Rota��o em torno de um ponto
		quat myQuat = quat(tb->quat_bar[3], tb->quat_bar[2], tb->quat_bar[1], tb->quat_bar[0]);
		glm::mat4 rotationMatrix = toMat4(myQuat);

		// Escala 
		glm::mat4 scaledMatrix;
		if (tb->bool_scaleUniform) {
			// Uniforme, todos valores passados s�o iguais
			scaledMatrix = glm::scale(tb->scaleValues.x, tb->scaleValues.x, tb->scaleValues.x);
			tb->scaleValues.y = tb->scaleValues.x;
			tb->scaleValues.z = tb->scaleValues.x;
		}
		else{
			//ou n�o-uniforme, onde um ou mais valores diferem, distorcendo o modelo
			scaledMatrix = glm::scale(tb->scaleValues.x, tb->scaleValues.y, tb->scaleValues.z);
		}

		// matriz com as opera��es mais basicas necess�rias
		glm::mat4 srt = translatedMatrix * rotationMatrix * scaledMatrix;

		// matriz com o efeito de shear ou uma identidade
		glm::mat4 shearMatrix;
		switch (whichShear(tb->bool_shear_x, tb->bool_shear_y, tb->bool_shear_z)){
			case -1 : 
				shearMatrix = glm::mat4(1.0f);
				break;
			case 0:
				shearMatrix = shearX3D(shearMatrix, tb->shear_xValues.y, tb->shear_xValues.z);
				break;
			case 1:
				shearMatrix = shearY3D(shearMatrix, tb->shear_yValues.x, tb->shear_yValues.z);
				break;
			case 2:
				shearMatrix = shearZ3D(shearMatrix, tb->shear_zValues.x, tb->shear_zValues.y);
				break;
		}

		// matriz supostamente refletida ou entao uma identidade
		glm::mat4 reflectionMatrix;
		switch (whichReflection(tb->bool_reflectX, tb->bool_reflectY, tb->bool_reflectZ)) {
			case -1:
				reflectionMatrix = glm::mat4(1.0f);
				break;
			case 0:
				reflectionMatrix = reflectionYZ();
				break;
			case 1:
				reflectionMatrix = reflectionXZ();
				break;
			case 2:
				reflectionMatrix = reflectionXY();
				break;
		}
		
		//m_currentMesh come�a em 0, se � trocado la em cima a mesh, troca tudo e fica tudo errado. A minha ideia era mandar somente com a linha de baixo
		//e as seguintes (com model.manager) comentadas, combinando tudo que � entrada numa linha s�
		modelManager.setModelMatrix(m_currentMesh, srt * glm::rotate((float)angleForArbitraryAxis, pointForTheArbitraryAxisValues) * reflectionMatrix * shearMatrix );
		
		modelManager.setModelMatrix(1, (glm::translate(2.0f, 0.0f, 0.0f) * scaledMatrix ));

		modelManager.setModelMatrix(2, teste * rotationMatrix * scaledMatrix);

		modelManager.setModelMatrix(3, (glm::translate(0.0f, 0.0f, +5.0f)) * reflectionMatrix);
		modelManager.setModelMatrix(4, (translatedMatrix + glm::translate(7.0f, 0.0f, 5.0f)) * glm::rotate((float)angleForArbitraryAxis, pointForTheArbitraryAxisValues));
		//modelManager.setModelMatrix(4, (translatedMatrix + glm::translate(7.0f, 0.0f, 5.0f) * reflectionXY()));
		
				
		cameraManager.getCamera(0)->setPositionAndLook(cameraTranslateValues, cameraLookAtValues);
		cameraManager.getCamera(0)->setRotation(toMat4(quat(camera_quat_bar[3], camera_quat_bar[2], camera_quat_bar[1], camera_quat_bar[0])));
		cameraManager.getCamera(0)->setRotation(glm::rotate((float)camera_angleForArbitraryAxis, camera_pointForTheArbitraryAxisValues));
		cameraManager.getCamera(0)->setFoV(cameraFoV);
		cameraManager.getCamera(0)->sendValuesToGPU();

		cameraManager.getCamera(1)->setPositionAndLook(cameraTranslateValues2, cameraLookAtValues2);
		cameraManager.getCamera(1)->setRotation(toMat4(quat(camera_quat_bar2[3], camera_quat_bar2[2], camera_quat_bar2[1], camera_quat_bar2[0])));
		cameraManager.getCamera(1)->setRotation(glm::rotate((float)camera_angleForArbitraryAxis2, camera_pointForTheArbitraryAxisValues2));
		cameraManager.getCamera(1)->setFoV(cameraFoV2);
		cameraManager.getCamera(1)->sendValuesToGPU();

		

		glm::vec3 lightPos = glm::vec3(4, 4, 4);
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		//modelManager.drawElements();
		//pretendia fazer como acima, mas n�o me parecia correto a projectionMatrix e a viewMatrix serem do modelManager
		for (int i = 0; i < modelManager.size(); i++) {
			Model *m = modelManager.getModel(i);
			glm::mat4 MVP = (*(cameraManager.getCamera())->getProjectionMatrix()) * (*cameraManager.getCamera()->getViewMatrix()) *  m->getModelMatrix();
			//glm::mat4 MVP = ProjectionMatrix * ViewMatrix *  m->getModelMatrix();
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
			m->drawElements();
		}

		// Draw tweak bars
		TwDraw();

		// Swap buffers
		glfwSwapBuffers(g_pWindow);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(g_pWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(g_pWindow) == 0);

	// Cleanup VBO and shader
	//model.getMesh()->cleanUpVBO();
	modelManager.terminate();

	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Terminate AntTweakBar and GLFW
	TwTerminate();
	glfwTerminate();

	return 0;
}
