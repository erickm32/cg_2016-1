#pragma once
#include <Camera.hpp>

Camera::Camera() {
	this->viewMatrix = glm::mat4(1.0f);
	this->projectionMatrix = glm::mat4(1.0f);
	this->viewMatrixID = -1; // se -1 digo que � invalido (?), � mais pra ter um construtor vazio pro vector
}


Camera::Camera(GLFWwindow* g_pWindow, GLuint viewMatrixID) {
	this->g_pWindow = g_pWindow;
	this->viewMatrixID = viewMatrixID;

	position = glm::vec3(0, 0, 5);
	horizontalAngle = 3.14f;
	verticalAngle = 0.0f;
	initialFoV = 45.0f;

	speed = 3.0f;
	mouseSpeed = 0.005f;

	near = 0.1f;
	far = 100.0f;
	aspect_ratio = 16.0f / 9.0f;
}

Camera::Camera(GLuint viewMatrixID) {
	this->viewMatrixID = viewMatrixID;
	this->projectionMatrix = glm::mat4(1.0);

	position = glm::vec3(0, 0, 5);
	horizontalAngle = 3.14f;
	verticalAngle = 0.0f;
	initialFoV = 45.0f;

	speed = 3.0f;
	mouseSpeed = 0.005f;

	near = 0.1f;
	far = 100.0f;
	aspect_ratio = 16.0f / 9.0f;
}

Camera::Camera(GLFWwindow* g_pWindow, GLuint viewMatrixID, glm::vec3 &camera_position, glm::vec3 &camera_lookAt) {
	this->g_pWindow = g_pWindow;
	this->viewMatrixID = viewMatrixID;

	position = camera_position;
	horizontalAngle = 3.14f;
	verticalAngle = 0.0f;
	initialFoV = 45.0f;

	speed = 3.0f;
	mouseSpeed = 0.005f;

	near = 0.1f;
	far = 100.0f;
	aspect_ratio = 16.0f / 9.0f;

	setPositionAndLook(camera_position, camera_lookAt); //nao tenho certeza
}

Camera::Camera(GLFWwindow* g_pWindow, GLuint viewMatrixID, glm::vec3 &camera_position, glm::vec3 &camera_lookAt, float FoV, float aspect_ratio, float near, float far) {
	this->g_pWindow = g_pWindow;
	this->viewMatrixID = viewMatrixID;

	position = camera_position;
	horizontalAngle = 3.14f;
	verticalAngle = 0.0f;
	initialFoV = FoV;

	speed = 3.0f;
	mouseSpeed = 0.005f;

	this->near = near;
	this->far = far;
	this->aspect_ratio = aspect_ratio;

	setPositionAndLook(camera_position, camera_lookAt); //nao tenho certeza
	setProjectionMatrix(glm::perspective(FoV, aspect_ratio, near, far));
}

void Camera::sendValuesToGPU() {
	glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);
}
glm::mat4* Camera::getViewMatrix() {
	return &viewMatrix;
}
void Camera::setViewMatrix(glm::mat4 &viewMatrix) {
	this->viewMatrix = viewMatrix;
}
glm::mat4* Camera::getProjectionMatrix() {
	return &projectionMatrix;
}

void Camera::setProjectionMatrix(glm::mat4 & projectionMatrix) {
	this->projectionMatrix = projectionMatrix;
}


void Camera::computeMatricesFromInputs(int nUseMouse, int nWidth, int nHeight) {

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();
	static int nLastUseMouse = 1;

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	double xpos = nWidth / 2, ypos = nHeight / 2;

	if (nUseMouse) {

		if (nLastUseMouse != nUseMouse)
			glfwSetCursorPos(g_pWindow, nWidth / 2, nHeight / 2);

		glfwGetCursorPos(g_pWindow, &xpos, &ypos);

		// Reset mouse position for next frame
		glfwSetCursorPos(g_pWindow, nWidth / 2, nHeight / 2);
	}

	nLastUseMouse = nUseMouse;

	// Compute new orientation
	horizontalAngle += mouseSpeed * float(nWidth / 2 - xpos);
	verticalAngle += mouseSpeed * float(nHeight / 2 - ypos);

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	direction = glm::vec3(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
		);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f / 2.0f),
		0,
		cos(horizontalAngle - 3.14f / 2.0f)
		);

	// Up vector
	up = glm::cross(right, direction);

	// Move forward
	if (glfwGetKey(g_pWindow, GLFW_KEY_UP) == GLFW_PRESS) {
		position += direction * deltaTime * speed;
	}
	// Move backward
	if (glfwGetKey(g_pWindow, GLFW_KEY_DOWN) == GLFW_PRESS) {
		position -= direction * deltaTime * speed;
	}
	// Strafe right
	if (glfwGetKey(g_pWindow, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		position += right * deltaTime * speed;
	}
	// Strafe left
	if (glfwGetKey(g_pWindow, GLFW_KEY_LEFT) == GLFW_PRESS) {
		position -= right * deltaTime * speed;
	}

	float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.

						   // Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
						   //ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.1f, 100.0f);
	projectionMatrix = glm::perspective(FoV, aspect_ratio, near, far);
	// Camera matrix
	viewMatrix = glm::lookAt(
		position,           // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
		);

	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
}

void Camera::setPositionAndLook(glm::vec3 & positionToTranslate, glm::vec3 & positionToLookAt) {
	viewMatrix = glm::lookAt(positionToTranslate, positionToLookAt , up);
}

void Camera::setFoV(float FoV) {
	initialFoV = FoV;
}

void Camera::setRotation(glm::mat4 &mat) {
	viewMatrix *= mat;
}
