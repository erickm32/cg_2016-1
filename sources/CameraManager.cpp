#pragma once
#include <CameraManager.hpp>

CameraManager::CameraManager(GLFWwindow * g_pWindow, GLuint programID) {
	this->g_pWindow = g_pWindow;
	this->programID = programID;
	this->activeCamera = -1;
}

void CameraManager::addCamera(Camera * camera) {
	cameras.push_back(*camera);
	activeCamera = cameras.size() - 1;
}

void CameraManager::addCamera(glm::vec3 & camera_position, glm::vec3 & camera_lookAt) {
	cameras.push_back(*new Camera(g_pWindow, glGetUniformLocation(programID, "V"), camera_position, camera_lookAt));
	activeCamera = cameras.size() - 1;
}

void CameraManager::addCamera(glm::vec3 & camera_position, glm::vec3 & camera_lookAt, float FoV, float aspect_ratio, float near, float far) {
	cameras.push_back(*new Camera(g_pWindow, glGetUniformLocation(programID, "V"), camera_position, camera_lookAt, FoV, aspect_ratio, near, far));
	activeCamera = cameras.size() - 1;
}

Camera * CameraManager::getCamera(int i) {
	if (i < cameras.size()) {
		return &cameras[i];
	}
	else {
		return NULL;
	}
}

Camera * CameraManager::getCamera() {
	return &cameras[activeCamera];
}

void CameraManager::setActiveCamera(int i) {
	if (i < cameras.size() && i >= 0) {
		activeCamera = i;
	}
}