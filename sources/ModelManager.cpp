#pragma once
#include <ModelManager.hpp>

Mesh* ModelManager::findMesh(std::string &meshId) {
	for (int i = 0; i < meshes.size(); i++) {
		if (*(meshes[i]->getId()) == meshId) {
			return meshes[i];
		}
	}
	return NULL;
}

void ModelManager::addModel(const char  *obj) {
	std::string idNewMesh = std::string(obj);
	//std::string novaMesh = std::string(obj);
	for (int i = 0; i < meshes.size(); i++) {
		if (*((meshes[i])->getId()) == idNewMesh) {
			Mesh *p = (meshes[i]);
			models.push_back(*new Model(p, models.size(), programID));
			return;
		}
	}
	Mesh *newMesh = new Mesh(obj, programID);
	meshes.push_back(newMesh);
	models.push_back(*new Model(newMesh, models.size(), programID));
}

void ModelManager::deleteModel(int &id) {
	int s = models.size();
	for (int i = 0; i < s; i++) {
		if (models[i].getId() == id) {
		}
	}
}

void ModelManager::drawElements() {
	for (Model m : models) {
		m.drawElements();
	}
}

void ModelManager::terminate() {
	for (Model m : models) {
		m.getMesh()->cleanUpVBO();
	}
}

glm::mat4 ModelManager::getModelMatrix(int id) {
	return models[id].getModelMatrix();
}

void ModelManager::setModelMatrix(int id, glm::mat4 &modelMatrix) {
	if (id < models.size()) {
		models[id].setModelMatrix(modelMatrix);
	}
}

Model* ModelManager::getModel(int id) {
	return &models[id];
}

const int ModelManager::size() {
	return models.size();
}

void ModelManager::changeModelMesh(int modelId, const char  *meshId) {
	std::string sMeshId = std::string(meshId);
	if (modelId < models.size()) {
		for (int i = 0; i < meshes.size(); i++) {
			if (*(meshes[i]->getId()) == sMeshId) {
				models[modelId].setMesh(findMesh(sMeshId));
			}
		}
	}
}
