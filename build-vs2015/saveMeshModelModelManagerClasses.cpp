class Mesh {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;

	GLuint vertexbuffer;
	GLuint uvbuffer;
	GLuint normalbuffer;
	GLuint elementbuffer;

	std::string id;
public:
	Mesh(const char  *obj) {
		// Read our .obj file
		//"mesh/suzanne.obj"
		//"mesh/cube.obj"
		bool res = loadOBJ(obj, vertices, uvs, normals);
		indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);
	
		// Load it into a VBO

		glGenBuffers(1, &vertexbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

		glGenBuffers(1, &uvbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

		glGenBuffers(1, &normalbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

		// Generate a buffer for the indices as well
		glGenBuffers(1, &elementbuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

		id = std::string(obj);
	}

	void drawElements() {
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,        // mode
			indices.size(),      // count
			GL_UNSIGNED_SHORT,   // type
			(void*)0             // element array buffer offset
			);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

	}

	// Cleanup VBO and shader
	void cleanUpVBO() {
		glDeleteBuffers(1, &vertexbuffer);
		glDeleteBuffers(1, &uvbuffer);
		glDeleteBuffers(1, &normalbuffer);
		glDeleteBuffers(1, &elementbuffer);
	}

	std::string *getId() {
		return &id;
	}
};

class Model {
	Mesh *mesh;
	glm::mat4 modelMatrix;
	GLuint ModelMatrixID;
	int id;
	

public:
	Model() {
		this->mesh = NULL;
		id = -1;
		this->modelMatrix = glm::mat4(1.0);
	}
	Model(const char  *obj, int id, GLuint programID) {
		this->mesh = new Mesh(obj);
		this->id = id;
		this->modelMatrix = glm::mat4(1.0);
		ModelMatrixID = glGetUniformLocation(programID, "M");
	}
	Model(Mesh* mesh, int id, GLuint programID) {
		this->mesh = mesh;
		this->id = id;
		this->modelMatrix = glm::mat4(1.0);
		ModelMatrixID = glGetUniformLocation(programID, "M");
	}

	const int getId() {
		return id;
	}

	void drawElements() {

		mesh->drawElements();
	}

	Mesh* getMesh() {
		return mesh;
	}

	void setMesh(Mesh *mesh) {
		this->mesh = mesh;
	}

	void setModelMatrix(const glm::mat4 &modelMatrix) {
		this->modelMatrix = modelMatrix;
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
	}

	glm::mat4 getModelMatrix() {
		return modelMatrix;
	}
};



class ModelManager {
	std::vector<Model> models;
	std::vector<Mesh*> meshes;

	Mesh* findMesh(std::string &meshId) {
		for (int i = 0; i < meshes.size(); i++) {
			if (*(meshes[i]->getId()) == meshId) {
				return meshes[i];
			}
		}
		return NULL;
	}

public:
	void addModel(const char  *obj, GLuint programID) {
		std::string idNewMesh = std::string(obj);
		//std::string novaMesh = std::string(obj);
		for (int i = 0; i < meshes.size(); i++) {
			if (*((meshes[i])->getId()) == idNewMesh) {
				Mesh *p = (meshes[i]);
				models.push_back(*new Model(p, models.size(), programID));
				return;
			}
		}
		Mesh *newMesh = new Mesh(obj);
		meshes.push_back(newMesh);
		models.push_back(*new Model(newMesh, models.size(), programID));
	}
	void deleteModel(int &id) {
		int s = models.size();
		for (int i = 0; i < s; i++) {
			if (models[i].getId() == id) {
			}
		}
	}
	void drawElements() {
		for (Model m : models) {
			m.drawElements();
		}
	}
	void terminate() {
		for (Model m : models) {
			m.getMesh()->cleanUpVBO();
		}
	}
	glm::mat4 getModelMatrix(int id) {
		return models[id].getModelMatrix();
	}
	void setModelMatrix(int id, glm::mat4 &modelMatrix) {
		if (id < models.size()) {
			models[id].setModelMatrix(modelMatrix);
		}
	}
	Model *getModel(int id) {
		return &models[id];
	}
	const int size() {
		return models.size();
	}
	void changeModelMesh(int modelId, const char  *meshId) {
		std::string sMeshId = std::string(meshId);
		if (modelId < models.size()) {
			for (int i = 0; i < meshes.size(); i++) {
				if (*(meshes[i]->getId()) == sMeshId) {
					models[modelId].setMesh(findMesh(sMeshId));
				}
			}
		}
	}
};

class Camera {
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
	GLuint viewMatrixID;
	GLuint projectionMatrixID;

public:
	Camera(GLuint viewMatrixID, GLuint projectionMatrixID) {
		this->viewMatrixID = viewMatrixID;
		this->projectionMatrixID = projectionMatrixID;
	}
	void sendValuesToGPU() {
		glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);
	}
	glm::mat4 *getViewMatrix() {
		return &viewMatrix;
	}
	void setViewMatrix(glm::mat4 &viewMatrix) {
		this->viewMatrix = viewMatrix;
	}
};
