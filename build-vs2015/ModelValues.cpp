struct ModelValues {
	glm::vec3 translateValues(0.1f, 0.1f, 0.1f);
	glm::vec3 scaleValues(1.0f, 1.0f, 1.0f);
	bool bool_scaleUniform = false;
	float quat_bar[4] = { 0, 0, 0, 1 };
	glm::vec3 shear_xValues(1.0f, 1.0f, 1.0f);
	bool bool_shear_x = false;
	glm::vec3 shear_yValues(1.0f, 1.0f, 1.0f);
	bool bool_shear_y = false;
	glm::vec3 shear_zValues(1.0f, 1.0f, 1.0f);
	bool bool_shear_z = false;
	glm::vec3 pointForTheArbitraryAxisValues(1.0f, 0.0f, 0.0f);
	double angleForArbitraryAxis = 0.0;
	bool bool_reflectX = false;
	bool bool_reflectY = false;
	bool bool_reflectZ = false;
};
