		teste.insert(*new Data(indexed_vertices[indices[i]]));
		neighbors[indices[i]].vertex = indexed_vertices[indices[i]];
		//neighbors[indices[i]].vertices_neighbors.insert(indexed_vertices[indices[i+1]]);
		neighbors[indices[i]].vertices_neighbors.push(indexed_vertices[indices[i + 1]]);
		//neighbors[indices[i]].vertices_neighbors.insert(indexed_vertices[indices[i+2]]);
		neighbors[indices[i]].vertices_neighbors.push(indexed_vertices[indices[i + 2]]);

		neighbors[indices[i+1]].vertex = indexed_vertices[indices[i+1]];
		//neighbors[indices[i+1]].vertices_neighbors.insert(indexed_vertices[indices[i]]);
		neighbors[indices[i + 1]].vertices_neighbors.push(indexed_vertices[indices[i]]);
		//neighbors[indices[i+1]].vertices_neighbors.insert(indexed_vertices[indices[i+2]]);
		neighbors[indices[i + 1]].vertices_neighbors.push(indexed_vertices[indices[i + 2]]);

		neighbors[indices[i+2]].vertex = indexed_vertices[indices[i+2]];
		//neighbors[indices[i+2]].vertices_neighbors.insert(indexed_vertices[indices[i]]);
		neighbors[indices[i + 2]].vertices_neighbors.push(indexed_vertices[indices[i]]);
		//neighbors[indices[i+2]].vertices_neighbors.insert(indexed_vertices[indices[i+1]]);
		neighbors[indices[i + 2]].vertices_neighbors.push(indexed_vertices[indices[i + 1]]);



		f << "Vertice: [" << neighbors[i].vertex.x << ", " << neighbors[i].vertex.y << ", " << neighbors[i].vertex.z << "]  \n" << neighbors[i].vertices_neighbors.size() << " Vizinhos: ";
		/*
		for (int j = 0; j < neighbors[i].vertices_neighbors.size(); j++) {
			f << "[" << neighbors[i].vertices_neighbors[j].x << ", " << neighbors[i].vertices_neighbors[j].y << ", " << neighbors[i].vertices_neighbors[j].z << "]" ;
			f << ", ";
		}
		*/
		//for (auto it = neighbors[i].vertices_neighbors.begin(); it != neighbors[i].vertices_neighbors.end(); ++it) {
		for(const auto& elem: neighbors[i].vertices_neighbors) {
			f << "[" << elem.x << ", " << elem.y << ", " << elem.z << "]";
			f << ", ";
		}
		f << std::endl << std::endl;



	/*
	std::ofstream file;
	file.open("distancias.txt");
	for (int i = 0; i < indexed_vertices.size() - 3; i+=3) {
		file << indices[i] << " " << indices[i+1] << " " << indices[i+2] << std::endl;
		file << "Distancia entre " << indices[i] << " e " << indices[i + 1] << ": " << distance(indexed_vertices[i], indexed_vertices[i + 1])
			<< ", entre " << indices[i] << " e " << indices[i + 2] << ": " << distance(indexed_vertices[i], indexed_vertices[i + 2])
			<< " e, entre " << indices[i + 1] << " e " << indices[i + 2] << ": " << distance(indexed_vertices[i + 1], indexed_vertices[i + 2]) << std::endl;
	}
	
	*/


	/*
	float getShortestDistance() {
		float d = 999;
		int i;
		for (i = 0; i < neighbor_distances.size(); i++) {
			if (neighbor_distances[i] <= d) {
				d = neighbor_distances[i];
			}
		}
		return d;
	}
	*/

	/*
class CompareDistances {
	bool reverse;
public:
	CompareDistances(const bool& revparam = false) {
		reverse = revparam;
	}
	bool operator() (const Data& lhs, const Data&rhs) const {
		if (reverse) return (lhs.getShortestDistance() > rhs.getShortestDistance());
		else return (lhs.getShortestDistance() < rhs.getShortestDistance());
	}
};
*/

/*
void writeVerticesToFile(glm::vec3 &indexed_vertices, glm::vec3 &indexed_uvs, glm::vec3 &indexed_normals) {
	std::ofstream file;
	file.open("saida.txt");
	for (int i = 0; i < indexed_vertices.size(); i++) {
		file << "i: " << i << " Vertice: [" << indexed_vertices[i].x << "," << indexed_vertices[i].y << "," << indexed_vertices[i].z << "]";
		file << "\t" << " Uvs: [" << indexed_uvs[i].x << "," << indexed_uvs[i].y << "]";
		file << "\t" << " Normals: [" << indexed_normals[i].x << "," << indexed_normals[i].y << "," << indexed_normals[i].z << "]" << std::endl;
	}
}
*/


/*
float get_areas(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
	float area = 1.0f; //shouldn't deault to 0, due to division by 0.0.
	float sphere_area = 0.0;
	float PI = 3.1415926;

	//First calculate the usual area
	glm::vec3 vec1 = b.point - a.point;
	glm::vec3 vec2 = c.point - a.point;

	glm::vec3 cross_prod = glm::cross(vec1, vec2);
	area = .5f*glm::sqrt(glm::dot(cross_prod, cross_prod));

	float orientation = 1.0;

	if (cross_prod[2]<0) {
		orientation = -1.0;
	}

	float bias = .01;
	area = (area + bias)* orientation;

	//Now from normals (ie points on gauss sphere), calculate the triangle area
	float a_temp, b_temp, c_temp, alpha, beta, gamma;
	a_temp = glm::acos(glm::dot(b.normal, c.normal));
	b_temp = glm::acos(glm::dot(a.normal, c.normal));
	c_temp = glm::acos(glm::dot(b.normal, a.normal));

	//exit if values lead to infinities
	if (glm::sin(a_temp) == 0.0f || glm::sin(b_temp) == 0.0f || glm::sin(c_temp) == 0.0f) {
		return;
	}

	//Law of cosines on the sphere to calculate angle between geodesic edges.
	alpha = glm::acos((glm::cos(a_temp) - (glm::cos(b_temp)*glm::cos(c_temp))) / (glm::sin(b_temp)*glm::sin(c_temp)));
	beta = glm::acos((glm::cos(b_temp) - (glm::cos(a_temp)*glm::cos(c_temp))) / (glm::sin(a_temp)*glm::sin(c_temp)));
	gamma = glm::acos((glm::cos(c_temp) - (glm::cos(b_temp)*glm::cos(a_temp))) / (glm::sin(b_temp)*glm::sin(a_temp)));


	//This follows from Gauss-Bonnet formula for geodesic triangles on sphere.
	sphere_area = alpha + beta + gamma - PI;

	float gauss_orientation = 1.0;

	vec1 = b.normal - a.normal;
	vec2 = c.normal - a.normal;

	cross_prod = glm::cross(vec1, vec2);

	if (cross_prod[2]<0) {
		gauss_orientation = -1.0;
	}

	sphere_area = sphere_area * gauss_orientation;

}
*/

/*
	std::ofstream f;
	f.open("vizinhosComIndicesApenas.txt");
	for (int i = 0; i < neighbors.size(); i++) {
		int a = neighbors[i].pVertex;
		if (a >= 0) {
			glm::vec3 v = indexed_vertices[a];
			f << "Para vertice: [" << v.x << ", " << v.y << ", " << v.z << "] os vizinhos são " << neighbors[i].qntNeighbors << ":\n";
			for (int j = 0; j < neighbors[i].qntNeighbors; j++) {
				auto it = (neighbors[i].neighbor_vertices[j]);
				glm::vec3 v = indexed_vertices[it];
				f << "[" << v.x << ", " << v.y << ", " << v.z << "] , ";
			}
			f << std::endl;
		}
	}
	*/


/*std::cout << "i: " << i << " indices[i]: " << indices[i] << "index: " << index 
							<< " d.pVertex " << d.pVertex
							//<< " indexed_vertices[indices[i]] " << "indexed_vertices[index] " << indexed_vertices[index] 
							<< std::endl;
							*/




/*
class Edge {
private:
	glm::vec3 a, b;
public:
	Edge(glm::vec3 &a, glm::vec3 &b) {
		this->a = a;
		this->b = b;
	}
	glm::vec3 getEdge_a() {
		return a;
	}
	glm::vec3 getEdge_b() {
		return b;
	}
	float getDistance() {
		return glm::distance(a, b);
	}

};

class Triangle {
private:
	glm::vec3 a, b, c;
	Edge *edge1, *edge2, *edge3;

public:
	Triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
		this->a = a;
		this->b = b;
		this->c = c;
		this->edge1 = new Edge(a, b);
		this->edge2 = new Edge(a, c);
		this->edge3 = new Edge(b, c);
	}

	glm::vec3 getA() { return a; }
	glm::vec3 getB() { return b; }
	glm::vec3 getC() { return c; }
	
	//std::vector<glm::vec3>& getVertices() { return std::vector<glm::vec3>(a, b, c); }

	void setA(glm::vec3 a) { this->a = a; }
	void setB(glm::vec3 b) { this->b = b; }
	void setC(glm::vec3 c) { this->c = c; }

	float getArea() {
		glm::vec3 v1, v2, v3;
		v1.x = b.x - a.x;
		v1.y = b.y - a.y;
		v1.z = b.z - a.z;

		v2.x = c.x - a.x;
		v2.y = c.y - a.y;
		v2.z = c.z - a.z;

		v3 = glm::cross(v1, v2);

		return 0.5f*sqrt(v3.x*v3.x + v3.y*v3.y + v3.z*v3.z);
	}
};



*/

/*
class MyCompare {
	bool reverse;
public:
	MyCompare(const bool& revparam = false) {
		reverse = revparam;
	}
	bool operator() (const float& lhs, const float&rhs) const
	{
		if (reverse) return (lhs>rhs);
		else return (lhs<rhs);
	}
};

struct Data {
	glm::vec3 vertex;
	std::priority_queue<glm::vec3,std::vector<glm::vec3>,MyCompare> vertices_neighbors;

	Data(glm::vec3) {
		this->vertex = vertex;
	}

	
};

namespace std {
		template <>
		struct hash<Data> {

			std::size_t operator()(const Data& d) const {
				using std::size_t;
				using std::hash;

				return ((hash<float>()(d.vertex.x) ^ (hash<float>()(d.vertex.y) << 1) ^ (hash<float>()(d.vertex.z) << 1)) >> 1);
			}
		};
	}

struct Data2 {
	int pVertex;
	std::unordered_set<int> neighbor_vertices;

	Data2(int pVertex) {
		this->pVertex = pVertex;
	}
	void const insert(const int pVertex) const{
		std::cout << pVertex << std::endl;
		neighbor_vertices.insert(pVertex);
	}
};

namespace std {
	template <>
	struct hash<Data2> {

		std::size_t operator()(const Data2& d) const {
			using std::size_t;
			using std::hash;

			//return (((hash<int>()(d.pVertex) ^ (hash<int>()(d.pVertex) << 1) )) >> 1);
			return hash<int>()(d.pVertex);
		}
	};
}
*/


float get_areas(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
	glm::vec3 v1, v2, v3;
	v1.x = b.x - a.x;
	v1.y = b.y - a.y;
	v1.z = b.z - a.z;

	v2.x = c.x - a.x;
	v2.y = c.y - a.y;
	v2.z = c.z - a.z;

	v3 = glm::cross(v1, v2);

	return 0.5f*sqrt(v3.x*v3.x + v3.y*v3.y + v3.z*v3.z);
}



		//teste
		MVP = ProjectionMatrix * ViewMatrix * modelManager.getModelMatrix(1);
		// Send our transformation to the currently bound shader,
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		//glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

		lightPos = glm::vec3(4, 4, 4);
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to user Texture Unit 0
		glUniform1i(TextureID, 0);
		modelManager.drawElements();
		//teste


// A variable for the current selection - will be updated by ATB
	MESH_TYPE m_currentMesh = SUZANNE;
	
	// Array of drop down items
	TwEnumVal Meshes[] = { { SUZANNE, "Suzanne" },{ CUBE, "Cube" },{ GOOSE, "Goose" } };

	// ATB identifier for the array
	TwType MeshTwType = TwDefineEnum("MeshType", Meshes, 3);

	// Link it to the tweak bar
	//TwAddVarCB(g_pToolBar, "Mesh", MeshTwType,NULL,NULL, &m_currentMesh, NULL); //tentativa de usar um callback pra tentar saber quando foi que o usuario selecionou outro modelo
	TwAddVarRW(g_pToolBar, "Mesh", MeshTwType, &m_currentMesh, NULL);

	// The second parameter is an optional name
	TwAddSeparator(g_pToolBar, "", NULL);		